# Group

**Students**

- Student 1
- Student 2
- Student 3

**Teaching Assistant**

- Juha Kälkäinen

# Initial Comments

- which tools / techniques could be used
- companies involved
- risks
- any other business?

# Situation room

Note: typically done (bi)weekly, always has the same structure. Should not take < 30 minutes, typically in person or virtual.

**202?-??-??**

- Done
- Done by next week
- Issues

**2021-09-12**

- Done
  * Initial meetup
  * Group members chosen
- Done by next week
  * Initial read up on tools that might be useful 
  * ??
- Issues
  * Student 1 unable to attend
  * ??

## Milestones

Note: student has to fill these after the first meeting. At least the beginning, and the end. (examples)

**202?-??-?? MILESTONE**: ???

**2021-09-01 MILESTONE**: 10:00 presentation of project at 10:00 in room TS361

**2021-09-15 MILESTONE**: Initial read up of tech used done

**2021-10-05 MILESTONE**: Backend ready